function[fatalfreak]=fftt(s);
fs=22050;
  T = 1/fs;                  
     L=length(s)  ;                 
  t = (0:L-1)*T ;
%      figure
%       plot(s);
%      xlabel('time (milliseconds)') 
      NFFT = 2^nextpow2(L) 
      sam=NFFT/2+1;
  Y = fft(s,NFFT)/L;
  k=linspace(0,1,sam);
 f = fs/2*k;
 [fi findx]=max(f);
 z=2*abs(Y(1:sam));
 [zi zindx]=max(z)
 fatalfreak=f(zindx);
%  figure
%  plot(f,2*abs(Y(1:NFFT/2+1))) ;
%  title('Single-Sided Amplitude Spectrum of y(t)') 
% xlabel('Frequency (Hz)') 
% ylabel('|Y(f)|')