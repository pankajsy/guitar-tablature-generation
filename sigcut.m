function [cut,fs,coll]=sigcut(b,cutvalue)
clc;
[x fs]=wavread(b);
x=x(:,1);
i=1;
coll=1;
x=x';

 while(x(i)==0)
    i=i+1;
 end
start=i;

zeroindex=1;
zerocount(zeroindex)=1;
zeroindex=zeroindex+1;
zerocount(zeroindex)=i;
temp=start;

while(i<length(x))
while(x(i)~=0 && i<length(x))
    i=i+1;
    cutcount=i;
    end
zeroindex=zeroindex+1;
while(x(i)==0 && i<length(x))
    i=i+1;
    zerocount(zeroindex)=i;
end
zerocount(zeroindex-1)=cutcount;
j=1;

if (zerocount(zeroindex)-zerocount(zeroindex-1)>cutvalue)
for random=temp:zerocount(zeroindex-1)
        cut(coll,j)=x(random);
        j=j+1;
end
coll=coll+1;
temp=zerocount(zeroindex);
end
end

j=1;
for random=temp:length(x)
    cut(coll,j)=x(random);
    j=j+1;
end
