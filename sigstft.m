function [funda]=sigstft(cut,coll,windowlength,fs)
for count=1:coll
x=cut(count,:);
w=hann(windowlength);
w=w';%1x2049

N=length(w);
n=length(x)/length(w);
n=round(n);
n=n-1;
% For Windowing operaton ;in each row of S , 2049 samples of x are stored
for i=1:n
for j=((((i-1)*length(w))+1):(i*length(w)))
s(i,(j-((i-1)*length(w))))=x(j);
end
end
% multimly each row of x by the window
%for FFt of each row of S
for i=1:n
windowedx(i,:)=s(i,:).*w(1,:);
f(i,:)=fft(windowedx(i,:),N);
end

u=abs(f);

%for finding the index j

for i=1:n
[maxo1(i),aplhabetj(i)]=max((u(i,:)));
end

alphabeti=1:n;

for i=1:n
alpha(i)=u(alphabeti(i),aplhabetj(i));
beta(i)=u(alphabeti(i),aplhabetj(i)+1);
gamma(i)=u(alphabeti(i),aplhabetj(i)+2);
numerator=alpha(i)-beta(i);
denominator=alpha(i)-(2*beta(i))+gamma(i);
impasse=numerator/denominator;
p(i)=0.5*impasse;
    newpeak(i)=beta(i)-((1/4)*(alpha(i)-gamma(i))*p(i));
end

[finalpeak,bin]=max(newpeak);
j=aplhabetj(bin);
funda(count)=j*fs/length(w);
%wavplay(x);
clear x j alphabetj u 
end
