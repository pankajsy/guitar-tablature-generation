
function varargout = tablature(varargin)
% TABLATURE M-file for tablature.fig
%      TABLATURE, by itself, creates a new TABLATURE or raises the existing
%      singleton*.
%
%      H = TABLATURE returns the handle to a new TABLATURE or the handle to
%      the existing singleton*.
%
%      TABLATURE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TABLATURE.M with the given input arguments.
%
%      TABLATURE('Property','Value',...) creates a new TABLATURE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before tablature_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to tablature_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help tablature

% Last Modified by GUIDE v2.5 13-Jan-2012 14:39:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @tablature_OpeningFcn, ...
                   'gui_OutputFcn',  @tablature_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before tablature is made visible.
function tablature_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to tablature (see VARARGIN)

% Choose default command line output for tablature
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
% refreshDisplays(action, handles, 1)

% UIWAIT makes tablature wait for user response (see UIRESUME)
% uiwait(handles.tables);


% --- Outputs from this function are returned to the command line.
function varargout = tablature_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in selectfile.
function selectfile_Callback(hObject, eventdata, handles)
% hObject    handle to selectfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName] = uigetfile('*.wav','Select the WAV-file');
name=strcat(PathName,FileName);
 set(handles.disp,'string','**********Guitar Tablature*********** ');
if (((get(handles.stft,'value')==0 ))&&(get(handles.fft,'value')==0 ))   % Get Tag of selected object
    set(handles.action,'string','Please select either stft or fft ');
elseif (((get(handles.stft,'value')==1 ))&&(get(handles.fft,'value')==0 ))
     %execute this code when stft radiobutton is selected
    [cut,fs,coll]=sigcut(name,3000); %[cut,fs,coll]=sigcut(b,cutvalue)
    [funda]=sigstft(cut,coll,8192,fs); %[funda]=sigstft(cut,coll,windowlength,fs)
    tab=tabgen(funda);
    set(handles.disp,'Data',tab);
  
elseif (((get(handles.stft,'value')==0 ))&&(get(handles.fft,'value')==1))
      
[x fs]=wavread(name);
fs = ['Sampling frequency: ' num2str(fs)];
% hObject    handle to play (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% =================================================

set(handles.action,'String',fs);
if (get(handles.fsamp,'value')==1 )
 fs = str2double(get(handles.samp1,'String'));
 fs = ['Sampling frequency: ' num2str(fs)];
set(handles.action,'String',fs);
end
% =================================================
% x=wavread(name);
fftx=fft(x,length(x));
fftx=abs(fftx);
axes(handles.plotaxis)
plot(x);
title('Signal-Time Domain');
xlabel('Time');
ylabel('Amplitude');

guidata(hObject, handles);

axes(handles.fftaxis)
plot(fftx);
title('Signal-Frequency Domain');
xlabel('Frequency');
ylabel('Amplitude');
guidata(hObject, handles);

% wavplay(x,fs);
[cut coll]=cutter(x);
clear  i
for i=1:coll
     s=cut(i,:);
     fundamental(i,:)=fftt(s);
     a=fundamental
     set(handles.tab,'Data',a); 
end
tabl=(tabfftf(fundamental));
    set(handles.disp,'String',tabl);
elseif(((get(handles.stft,'value')==1 ))&&(get(handles.fft,'value')==1 ))
    set(handles.action,'string','Please select either stft or fft ');
end
%updates the handles structure
guidata(hObject, handles);






function play_Callback(hObject, eventdata, handles)
[FileName,PathName] = uigetfile('*.wav','Select the WAV-file');
name=strcat(PathName,FileName);
[x fs]=wavread(name);
wavplay(x,fs);


function disp_Callback(hObject, eventdata, handles)
% hObject    handle to disp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of disp as text
%        str2double(get(hObject,'String')) returns contents of disp as a double


% --- Executes during object creation, after setting all properties.
function disp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to disp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in fft.
function fft_Callback(hObject, eventdata, handles)
% hObject    handle to fft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fft


% --- Executes on button press in stft.
function stft_Callback(hObject, eventdata, handles)
% hObject    handle to stft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of stft


% --- Executes on button press in chart.
function chart_Callback(hObject, eventdata, handles)
% hObject    handle to chart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.action,'string','Frequency chart is been displayed');
A=imread('frequency.jpg');
figure;
image(A);

% --- Executes on button press in close.
function close_Callback(hObject, eventdata, handles)
% hObject    handle to close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.action, 'string', 'Closing the GUI')
display bye
close(gcbf);









% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), ...
           get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
xmax=0.18
set(hObject,'Min',xmax*0.5,'Max',xmax)
set(hObject,'Value',xmax*0.6)


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

sliderVal    = get(hObject,'Value');
sliderStatus = ['threshold: ' num2str(sliderVal)];
set(handles.action, 'string', sliderStatus)
% view(sliderVal, 30)






function samp1_Callback(hObject, eventdata, handles)
% hObject    handle to samp1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of samp1 as text
%        str2double(get(hObject,'String')) returns contents of samp1 as a double


% --- Executes during object creation, after setting all properties.
function samp1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to samp1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in fsamp.
function fsamp_Callback(hObject, eventdata, handles)
% hObject    handle to fsamp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fsamp


% --- Executes during object creation, after setting all properties.
function tab_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tab (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in helpb.
function helpb_Callback(hObject, eventdata, handles)
% hObject    handle to helpb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 HelpPath = which('tablature_help.html');
  web(HelpPath); 


% --- Executes on selection change in algorithm.
function algorithm_Callback(hObject, eventdata, handles)
% hObject    handle to algorithm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns algorithm contents as cell array
%        contents{get(hObject,'Value')} returns selected item from algorithm
val = get(hObject,'Value');
str = get(hObject, 'String');
switch str{val};
case 'Fast fourier transform' % User selects peaks
	set(handles.action, 'string', 'Fast fourier transform');
case 'Short time fourier transform' % User selects membrane
	set(handles.action, 'string', 'Short time fourier transform');
case 'Wavelet transform' % User selects sinc
	set(handles.action, 'string', 'Wavelet transform');
end
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function algorithm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to algorithm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
